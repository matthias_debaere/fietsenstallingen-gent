import axios from 'axios'

/* Shared instance */
// eslint-disable-next-line @typescript-eslint/no-unsafe-member-access -- Disable unsafe access warning
axios.defaults.headers.common['Content-Type'] = 'application/json'

export const stadGentApi = axios.create({
  baseURL: 'https://data.stad.gent/api/records/1.0/search/',
})