/* eslint-disable sort-keys-fix/sort-keys-fix -- We provide our own sorting here. */
// eslint-disable-next-line import/no-extraneous-dependencies, import/no-commonjs, @typescript-eslint/no-var-requires -- Disabled because we're keeping this file as a commonJS module.
const restrictedGlobals = require('eslint-restricted-globals')


// eslint-disable-next-line import/no-commonjs -- Disabled because we're keeping this file as a commonJS module
module.exports = {
  ignorePatterns: ['node_modules'],
  env: {
    browser: true, // browser global variables.
    commonjs: true, // CommonJS global variables and CommonJS scoping (use this for browser-only code that uses Browserify/WebPack).
    es6: true, // enable all ECMAScript 6 features except for modules (this automatically sets the ecmaVersion parser option to 6).
  },
  extends: ['plugin:@typescript-eslint/recommended'],
  globals: {
    __DEV__: true,
    __dirname: false,
    __fbBatchedBridgeConfig: false,
    alert: false,
    cancelAnimationFrame: false,
    cancelIdleCallback: false,
    clearImmediate: true,
    clearInterval: false,
    clearTimeout: false,
    console: false,
    document: false,
    ErrorUtils: false,
    escape: false,
    Event: false,
    EventTarget: false,
    exports: false,
    fetch: false,
    FormData: false,
    global: false,
    Headers: false,
    Intl: false,
    Map: true,
    module: false,
    navigator: false,
    process: false,
    Promise: true,
    requestAnimationFrame: true,
    requestIdleCallback: true,
    require: false,
    Set: true,
    setImmediate: true,
    setInterval: false,
    setTimeout: false,
    URL: false,
    URLSearchParams: false,
    WebSocket: true,
    window: false,
    XMLHttpRequest: false,
  },
  overrides: [
    {
      files: ['*.{spec,test}.{js,ts,tsx}', '**/__{mocks,tests}__/**/*.{js,ts,tsx}'],
      env: {
        'jest': true,
        'jest/globals': true,
      },
      rules: {},
    },
    {
      files: ['**/src/**/api/**/*.{js,ts}'],
      env: {
        node: true,
      },
      rules: {
        'no-restricted-imports': 'off',
      },
    },
    {
      files: ['*.ts', '*.tsx'],
      parser: '@typescript-eslint/parser',
      parserOptions: {
        project: ['./tsconfig.eslint.json'],
      },
      extends: [
        'plugin:@typescript-eslint/eslint-recommended',
        'plugin:@typescript-eslint/recommended-requiring-type-checking',
        'plugin:import/typescript', // https://github.com/benmosher/eslint-plugin-import#typescript
      ],
      rules: {
        '@typescript-eslint/array-type': 'warn',
        '@typescript-eslint/ban-ts-comment': [
          'warn',
          {
            'ts-expect-error': 'allow-with-description',
            'ts-ignore': 'allow-with-description',
            'ts-nocheck': 'allow-with-description',
            'ts-check': 'allow-with-description',
          },
        ],
        '@typescript-eslint/ban-tslint-comment': 'error', // tslint and their disable comments shouldn't be used anymore
        'brace-style': 'off', // turn off eslint base rule for @typescript-eslint/brace-style
        '@typescript-eslint/brace-style': ['error'],
        '@typescript-eslint/class-literal-property-style': ['warn', 'fields'],
        'comma-dangle': 'off', // turn off eslint base rule for @typescript-eslint/comma-dangle
        '@typescript-eslint/comma-dangle': [
          'warn',
          'always-multiline',
          {
            arrays: 'always-multiline',
            objects: 'always-multiline',
            imports: 'always-multiline',
            exports: 'always-multiline',
            functions: 'always-multiline',
            // typescript specific options
            enums: 'warn',
            generics: 'off',
            tuples: 'off',
          },
        ],
        'comma-spacing': 'off', // turn off eslint base rule for @typescript-eslint/comma-spacing
        '@typescript-eslint/comma-spacing': [
          'error',
          {
            before: false,
            after: true,
          },
        ],
        '@typescript-eslint/consistent-indexed-object-style': ['warn', 'record'],
        '@typescript-eslint/consistent-type-assertions': [
          'warn',
          {
            assertionStyle: 'as',
            objectLiteralTypeAssertions: 'never',
          },
        ],
        '@typescript-eslint/consistent-type-imports': [
          'warn',
          {
            prefer: 'no-type-imports',
          },
        ],
        'default-param-last': 'off', // turn off eslint base rule for @typescript-eslint/default-param-last
        '@typescript-eslint/default-param-last': ['error'],
        'dot-notation': 'off', // turn off eslint base rule for @typescript-eslint/dot-notation
        '@typescript-eslint/dot-notation': [
          'warn',
          {
            allowKeywords: true,
            allowPattern: '^[a-z]+(_[a-z]+)+$',
            // typescript specific
            allowPrivateClassPropertyAccess: false,
            allowProtectedClassPropertyAccess: false,
          },
        ],
        '@typescript-eslint/explicit-function-return-type': [
          'warn',
          {
            allowExpressions: true,
            allowTypedFunctionExpressions: true,
            allowDirectConstAssertionInArrowFunctions: true,
            allowConciseArrowFunctionExpressionsStartingWithVoid: true,
          },
        ],
        '@typescript-eslint/explicit-member-accessibility': [
          'warn',
          {
            accessibility: 'explicit',
          },
        ],
        'func-call-spacing': 'off', // turn off eslint base rule for @typescript-eslint/func-call-spacing
        '@typescript-eslint/func-call-spacing': ['error'],
        'indent': 'off', // turn off eslint base rule for @typescript-eslint/indent
        '@typescript-eslint/indent': [ // let's see what happens (https://github.com/typescript-eslint/typescript-eslint/issues/1824)
          'warn',
          2,
          {
            ArrayExpression: 'first',
            CallExpression: {
              arguments: 'first',
            },
            flatTernaryExpressions: false,
            FunctionDeclaration: {
              body: 1,
              parameters: 'first',
            },
            ignoreComments: false,
            ImportDeclaration: 'first',
            MemberExpression: 1,
            ObjectExpression: 'first',
            offsetTernaryExpressions: true,
            outerIIFEBody: 1,
            SwitchCase: 1,
            VariableDeclarator: 'first',
            ignoredNodes: [
              'JSXElement',
              // 'JSXElement > *', // commented so that declarations of children inside other JSX components are properly indented.
              'JSXAttribute',
              'JSXIdentifier',
              'JSXNamespacedName',
              'JSXMemberExpression',
              'JSXSpreadAttribute',
              // 'JSXExpressionContainer', // commented so that attributes containing objects are properly indented.
              'JSXOpeningElement',
              'JSXClosingElement',
              'JSXText',
              'JSXEmptyExpression',
              'JSXSpreadChild',
            ], // https://github.com/yannickcr/eslint-plugin-react/issues/1679#issuecomment-363908562
          },
        ],
        '@typescript-eslint/init-declarations': 'off', // Not used
        'keyword-spacing': 'off', // turn off eslint base rule for @typescript-eslint/keyword-spacing
        '@typescript-eslint/keyword-spacing': [
          'warn',
          {
            before: true,
            after: true,
          },
        ],
        'lines-between-class-members': 'off', // turn off eslint base rule for @typescript-eslint/lines-between-class-members
        '@typescript-eslint/lines-between-class-members': [
          'warn',
          'always',
          {
            exceptAfterSingleLine: false,
            // typescript specific
            exceptAfterOverload: true,
          },
        ],
        '@typescript-eslint/member-delimiter-style': [
          'warn',
          {
            multiline: {
              delimiter: 'none',
            },
            singleline: {
              delimiter: 'semi',
              requireLast: false,
            },
          },
        ],
        '@typescript-eslint/member-ordering': 'warn',
        '@typescript-eslint/method-signature-style': ['warn', 'property'], // Use 'property' to enforce maximum correctness together with TypeScript's strict mode.
        'camelcase': 'off', // turn off eslint base rule for @typescript-eslint/naming-convention
        '@typescript-eslint/naming-convention': [
          'warn',
          {
            selector: 'default',
            format: ['camelCase'],
          },

          {
            selector: 'variable',
            format: [
              'camelCase',
              'PascalCase',
              'UPPER_CASE',
            ],
          },
          {
            selector: 'variable',
            types: ['boolean'],
            format: ['PascalCase'], // the prefix is trimmed before format is validated, thus PascalCase must be used to allow variables such as isEnabled.
            prefix: [
              'is',
              'should',
              'has',
              'can',
              'did',
              'will',
            ],
          },
          {
            selector: ['function'],
            format: ['camelCase'],
            leadingUnderscore: 'allow',
          },
          {
            selector: 'parameter',
            format: ['camelCase', 'PascalCase'],
            leadingUnderscore: 'allow',
          },

          {
            selector: 'memberLike',
            modifiers: ['private'],
            format: ['camelCase'],
            leadingUnderscore: 'require',
          },

          {
            selector: 'typeLike',
            format: ['PascalCase'],
          },
          {
            selector: 'typeParameter',
            format: ['PascalCase'],
            prefix: ['T', 'G'],
          },
          {
            selector: 'typeProperty',
            format: ['camelCase', 'UPPER_CASE'],
          },
          {
            selector: 'interface',
            format: ['PascalCase'],
            trailingUnderscore: 'allow',
            custom: {
              regex: '^I[A-Z]|ProcessEnv',
              match: true,
            },
          },
        ],
        '@typescript-eslint/no-confusing-non-null-assertion': 'off',
        '@typescript-eslint/no-confusing-void-expression': [
          'error',
          {
            ignoreArrowShorthand: true,
          },
        ],
        'no-dupe-class-members': 'off', // turn off eslint base rule for @typescript-eslint/no-dupe-class-members
        '@typescript-eslint/no-dupe-class-members': 'warn',
        '@typescript-eslint/no-implicit-any-catch': 'warn',

        'no-duplicate-imports': 'off', // turn off eslint base rule for @typescript-eslint/no-duplicate-imports
        '@typescript-eslint/no-duplicate-imports': 'warn',
        '@typescript-eslint/no-dynamic-delete': 'warn',
        'no-empty-function': 'off', // turn off eslint base rule for @typescript-eslint/no-empty-function
        '@typescript-eslint/no-empty-function': 'off', // no warning because most of the times it is pretty clear why we use empty functions
        '@typescript-eslint/no-empty-interface': [
          'warn',
          {
            allowSingleExtends: false,
          },
        ],
        '@typescript-eslint/no-explicit-any': [
          'warn',
          {
            fixToUnknown: true,
          },
        ],
        'no-extra-parens': 'off', // turn off eslint base rule for @typescript-eslint/no-extra-parens
        '@typescript-eslint/no-extra-parens': [
          'warn',
          'all',
          {
            ignoreJSX: 'multi-line',
            nestedBinaryExpressions: false,
            enforceForArrowConditionals: false,
          },
        ],
        'no-extra-semi': 'off', // turn off eslint base rule for @typescript-eslint/no-extra-semi
        '@typescript-eslint/no-extra-semi': 'warn',
        '@typescript-eslint/no-extraneous-class': 'warn',
        // '@typescript-eslint/no-floating-promises': 'off', // https://github.com/typescript-eslint/typescript-eslint/blob/v4.3.0/packages/eslint-plugin/docs/rules/no-floating-promises.md
        '@typescript-eslint/no-inferrable-types': 'off', // no warning because it doesn't hurt to declare explicit type declarations on parameters, variables and properties where the type can be easily inferred from its value.
        'no-invalid-this': 'off',
        '@typescript-eslint/no-invalid-this': [
          'warn',
          {
            capIsConstructor: true,
          },
        ],
        '@typescript-eslint/no-invalid-void-type': [
          'warn',
          {
            allowInGenericTypeArguments: true,
          },
        ],
        'no-loop-func': 'off', // turn off eslint base rule for @typescript-eslint/no-loop-func
        '@typescript-eslint/no-loop-func': 'warn',
        'no-loss-of-precision': 'off', // turn off eslint base rule for @typescript-eslint/no-loss-of-precision
        '@typescript-eslint/no-loss-of-precision': 'error',
        '@typescript-eslint/no-parameter-properties': 'warn',
        'no-redeclare': 'off', // turn off eslint base rule for @typescript-eslint/no-loss-of-precision
        '@typescript-eslint/no-redeclare': [
          'error',
          {
            builtinGlobals: true,
            ignoreDeclarationMerge: true,
          },
        ],
        'no-restricted-syntax': [
          'error',
          {
            selector: 'TSEnumDeclaration',
            message: "Don't declare enums, use unions of symbol singleton types, or object literals with string-valued properties (e.g. const colorEnum = { 'red': 'red', 'blue': 'blue'} as const) instead.",  // see https://2ality.com/2020/02/enum-alternatives-typescript.html#unions-of-symbol-singleton-types or https://2ality.com/2020/02/enum-alternatives-typescript.html#object-literals-with-string-valued-properties for the alternatives and https://www.techatbloomberg.com/blog/10-insights-adopting-typescript-at-scale (first point) for the reason.
          },
        ],
        '@typescript-eslint/no-require-imports': 'warn',
        'no-shadow': 'off', // turn off eslint base rule for @typescript-eslint/no-shadow
        '@typescript-eslint/no-shadow': [
          'warn',
          {
            ignoreTypeValueShadow: false, // It can be confusing to use the same name as a type and as a variable identifier.
          },
        ],
        'no-throw-literal': 'off', // turn off eslint base rule for @typescript-eslint/no-throw-literal
        '@typescript-eslint/no-throw-literal': 'warn',
        '@typescript-eslint/no-type-alias': [
          'warn',
          {
            allowAliases: 'always',
            allowConditionalTypes: 'always',
            allowLiterals: 'always',
            allowCallbacks: 'always',
          },
        ],
        '@typescript-eslint/no-unnecessary-boolean-literal-compare': 'warn',
        '@typescript-eslint/no-unnecessary-condition': 'error',
        '@typescript-eslint/no-unnecessary-qualifier': 'warn',
        '@typescript-eslint/no-unnecessary-type-arguments': 'off', // no warning because more verbosity can be better (but is not enforced because we also want readability)
        '@typescript-eslint/no-unnecessary-type-assertion': 'warn',
        '@typescript-eslint/no-unnecessary-type-constraint': 'warn',
        '@typescript-eslint/no-unsafe-assignment': 'warn',
        '@typescript-eslint/no-unsafe-call': 'warn',
        '@typescript-eslint/no-unsafe-member-access': 'warn',
        'no-unused-expressions': 'off', // turn off eslint base rule for @typescript-eslint/no-unused-expressions
        '@typescript-eslint/no-unused-expressions': [
          'error',
          {
            allowShortCircuit: true,
            allowTernary: true,
            allowTaggedTemplates: true,
          },
        ],
        'no-unused-vars': 'off', // turn off eslint base rule for @typescript-eslint/no-unused-vars
        '@typescript-eslint/no-unused-vars': [
          'warn',
          {
            caughtErrors: 'all',
            ignoreRestSiblings: true,
            vars: 'all',
            args: 'none',
          },
        ],
        'no-use-before-define': 'off', // turn off eslint base rule for @typescript-eslint/no-use-before-define
        '@typescript-eslint/no-use-before-define': 'warn',
        'no-useless-constructor': 'off', // turn off eslint base rule for @typescript-eslint/no-useless-constructor
        '@typescript-eslint/no-useless-constructor': 'warn',
        '@typescript-eslint/non-nullable-type-assertion-style': 'warn',
        'object-curly-spacing': 'off', // turn off eslint base rule for @typescript-eslint/object-curly-spacing
        '@typescript-eslint/object-curly-spacing': [
          'error',
          'always',
          {
            arraysInObjects: false,
            objectsInObjects: false,
          },
        ],
        '@typescript-eslint/prefer-enum-initializers': 'off', // turned off, not using enums anyways
        '@typescript-eslint/prefer-for-of': 'off', // no preference
        '@typescript-eslint/prefer-function-type': 'off', // no preference
        '@typescript-eslint/prefer-includes': 'warn', // might conflict with 'unicorn/prefer-includes' rule
        '@typescript-eslint/prefer-literal-enum-member': 'off', // turned off, not using enums anyways
        '@typescript-eslint/prefer-nullish-coalescing': 'warn',
        '@typescript-eslint/prefer-optional-chain': 'warn',
        '@typescript-eslint/prefer-readonly-parameter-types': 'off', // this is more for libraries
        '@typescript-eslint/prefer-readonly': 'warn',
        '@typescript-eslint/prefer-reduce-type-parameter': 'warn',
        '@typescript-eslint/prefer-string-starts-ends-with': 'warn', // might conflict with 'unicorn/prefer-string-starts-ends-with' rule
        '@typescript-eslint/prefer-ts-expect-error': 'warn',
        '@typescript-eslint/promise-function-async': 'warn', // might conflict with 'no-async-promise-executor' rule, let's see what happens
        'quotes': 'off', // turn off eslint base rule for @typescript-eslint/quotes
        '@typescript-eslint/quotes': [
          'warn',
          'single',
          {
            avoidEscape: true,
            allowTemplateLiterals: true,
          },
        ],
        '@typescript-eslint/require-array-sort-compare': [
          'warn',
          {
            ignoreStringArrays: true,
          },
        ],
        'require-await': 'off', // turn off eslint base rule for @typescript-eslint/require-await
        '@typescript-eslint/require-await': 'warn',
        '@typescript-eslint/restrict-plus-operands': [
          'warn',
          {
            checkCompoundAssignments: true,
          },
        ],
        '@typescript-eslint/restrict-template-expressions': [
          'warn',
          {
            allowBoolean: true,
            allowNumber: true,
          },
        ],
        'semi': 'off', // turn off eslint base rule for @typescript-eslint/semi
        '@typescript-eslint/semi': ['warn', 'never'],
        '@typescript-eslint/sort-type-union-intersection-members': 'warn',
        'space-before-function-paren': 'off', // turn off eslint base rule for @typescript-eslint/space-before-function-paren
        '@typescript-eslint/space-before-function-paren': [
          'warn',
          {
            anonymous: 'never',
            named: 'never',
            asyncArrow: 'always',
          },
        ],
        'space-infix-ops': 'off', // turn off eslint base rule for @typescript-eslint/space-infix-ops
        '@typescript-eslint/space-infix-ops': 'warn',
        '@typescript-eslint/strict-boolean-expressions': 'warn',
        '@typescript-eslint/switch-exhaustiveness-check': 'error',
        '@typescript-eslint/type-annotation-spacing': 'warn',
        '@typescript-eslint/typedef': 'off', // already handled by TypeScript's compiler options, particularly --noImplicitAny and/or --strictPropertyInitialization
        '@typescript-eslint/unified-signatures': 'warn',
      },
    },
  ],
  parserOptions: {
    ecmaVersion: 2020,
    ecmaFeatures: {
      jsx: true,
    },
    sourceType: 'module',
  },
  plugins: [
    '@typescript-eslint',
    'eslint-comments',
    'import',
    'jest',
    'jsx-a11y',
    'modules-newline',
    'promise',
    'react-hooks',
    'react',
    'sort-keys-fix',
    'unicorn',
  ],
  rules: {
    /* ESLINT RULES (https://github.com/eslint/eslint/tree/master/docs/rules) */
    'array-bracket-newline': [
      'warn',
      {
        multiline: true, // true so when an object with multiple properties has a newline for each property, the array follows the newlines for each item to improve readability.
        minItems: 3, // 3 works better with react hooks destructuring
      },
    ],
    'array-bracket-spacing': ['warn', 'never'],
    'array-callback-return': [
      'warn',
      {
        allowImplicit: true,
        checkForEach: true,
      },
    ],
    'array-element-newline': [
      'warn',
      {
        multiline: true, // true so when an object with multiple properties has a newline for each property, the array follows the newlines for each item to improve readability.
        minItems: 3, // 3 works better with react hooks destructuring
      },
    ],
    'arrow-body-style': ['warn', 'as-needed'],
    'arrow-parens': ['warn', 'always'],
    'arrow-spacing': [
      'warn',
      {
        before: true,
        after: true,
      },
    ],
    'block-scoped-var': 'warn',
    'block-spacing': ['warn', 'always'],
    'brace-style': ['warn', '1tbs'],
    'camelcase': [
      'warn',
      {
        ignoreDestructuring: false,
        ignoreImports: false,
        properties: 'always',
      },
    ],
    'capitalized-comments': 'off', // Behaves annoying when briefly commenting code to test something and it automatically capitalizes the first word. An ignore regex could solve this but is not worth the time.
    'class-methods-use-this': 'warn',
    'comma-dangle': ['warn', 'always-multiline'],
    'comma-spacing': [
      'error',
      {
        before: false,
        after: true,
      },
    ],
    'comma-style': ['error', 'last'],
    'complexity': 'off',
    'computed-property-spacing': ['warn', 'never'],
    'consistent-return': [
      'warn',
      {
        treatUndefinedAsUnspecified: false, // false = allow explicitly returning undefined
      },
    ],
    'consistent-this': ['warn', 'that'],
    'constructor-super': 'error',
    'curly': [
      'warn',
      'multi-line',
      'consistent',
    ],
    'default-case-last': 'warn',
    'default-case': [
      'warn',
      {
        commentPattern: '^no\\sdefault',
      },
    ],
    'default-param-last': ['error'],
    'dot-location': ['error', 'property'],
    'dot-notation': [
      'warn',
      {
        allowKeywords: true,
        allowPattern: '^[a-z]+(_[a-z]+)+$',
      },
    ],
    'eol-last': ['warn', 'always'],
    'eqeqeq': ['warn', 'smart'],
    'for-direction': 'warn',
    'func-call-spacing': ['warn', 'never'],
    'func-name-matching': 'off',
    'func-names': ['warn', 'as-needed'],
    'func-style': [
      'warn',
      'declaration',
      {
        allowArrowFunctions: true,
      },
    ],
    'function-call-argument-newline': ['warn', 'consistent'],
    'function-paren-newline': ['warn', 'consistent'],
    'generator-star-spacing': [
      'warn',
      {
        before: true,
        after: false,
      },
    ],
    'getter-return': [
      'error',
      {
        allowImplicit: false,
      },
    ],
    'grouped-accessor-pairs': ['warn', 'getBeforeSet'],
    'guard-for-in': 'error',
    'id-denylist': [
      'error',
      'any',
      'Number',
      'number',
      'String',
      'string',
      'Boolean',
      'boolean',
      'Undefined',
      // 'undefined', // commented because of buggy behaviour
    ],
    'id-length': [
      'warn',
      {
        exceptions: [
          'a',
          'b',
          'e',
          'i',
          '_',
        ],
        min: 2,
        properties: 'always',
      },
    ],
    'id-match': 'off',
    'implicit-arrow-linebreak': ['warn', 'beside'],
    'indent': [
      'warn',
      2,
      {
        ArrayExpression: 'first',
        CallExpression: {
          arguments: 'first',
        },
        flatTernaryExpressions: false,
        FunctionDeclaration: {
          body: 1,
          parameters: 'first',
        },
        ignoreComments: false,
        ImportDeclaration: 'first',
        MemberExpression: 1,
        ObjectExpression: 'first',
        offsetTernaryExpressions: true,
        outerIIFEBody: 1,
        SwitchCase: 1,
        VariableDeclarator: 'first',
        ignoredNodes: [
          'JSXElement',
          // 'JSXElement > *', // commented so that declarations of children inside other JSX components are properly indented.
          'JSXAttribute',
          'JSXIdentifier',
          'JSXNamespacedName',
          'JSXMemberExpression',
          'JSXSpreadAttribute',
          // 'JSXExpressionContainer', // commented so that attributes containing objects are properly indented.
          'JSXOpeningElement',
          'JSXClosingElement',
          'JSXText',
          'JSXEmptyExpression',
          'JSXSpreadChild',
        ], // https://github.com/yannickcr/eslint-plugin-react/issues/1679#issuecomment-363908562
      },
    ],
    'init-declarations': 'off', // turn off to be able to have an empty declaration as a way of assigning undefined without conflicting with no-undef-init
    'jsx-quotes': ['warn', 'prefer-single'],
    'key-spacing': [
      'warn',
      {
        mode: 'strict',
        // Enable these options below to align values and colons like below:
        // 'singleLine': {
        //   'beforeColon': false,
        //   'afterColon' : true,
        //   'mode'       : 'minimum',
        // },
        // 'multiLine': {
        //   'beforeColon': false,
        //   'afterColon' : true,
        //   'mode'       : 'minimum',
        // },
        // 'align': {
        //   'beforeColon': false, // Or true
        //   'afterColon' : true,
        //   'on'         : 'colon',
        // },
      },
    ],
    'keyword-spacing': [
      'warn',
      {
        before: true,
        after: true,
      },
    ],
    'line-comment-position': 'off',
    'linebreak-style': 'off',
    'lines-around-comment': [
      'warn',
      {
        beforeBlockComment: true,
        allowBlockStart: true,
        allowBlockEnd: true,
        allowObjectStart: true,
        allowObjectEnd: true,
        allowArrayStart: true,
        allowArrayEnd: true,
        allowClassStart: true,
        allowClassEnd: true,
      },
    ],
    'lines-between-class-members': [
      'warn',
      'always',
      {
        exceptAfterSingleLine: false,
      },
    ],
    'max-classes-per-file': 'off',
    'max-depth': 'off',
    'max-len': 'off',
    'max-lines-per-function': 'off',
    'max-lines': 'off',
    'max-nested-callbacks': ['warn', 10],
    'max-params': 'off',
    'max-statements-per-line': 'off',
    'max-statements': 'off',
    'multiline-comment-style': 'off', // ['warn', 'starred-block'],
    'multiline-ternary': ['warn', 'always-multiline'],
    'new-cap': 'off',
    'new-parens': 'warn',
    'newline-after-var': 'off', // Doesn't always work correctly, probably the reason why it's deprecated. TODO: Replace with 'padding-line-between-statements' rule (https://github.com/eslint/eslint/blob/master/docs/rules/padding-line-between-statements.md)
    'newline-per-chained-call': [
      'warn',
      {
        ignoreChainWithDepth: 2,
      },
    ],
    'no-alert': 'error',
    'no-array-constructor': 'warn',
    'no-async-promise-executor': 'error',
    'no-await-in-loop': 'warn',
    'no-bitwise': 'error',
    'no-caller': 'error',
    'no-case-declarations': 'off',
    'no-class-assign': 'warn',
    'no-compare-neg-zero': 'error',
    'no-cond-assign': ['warn', 'except-parens'],
    'no-confusing-arrow': [
      'error',
      {
        allowParens: true,
      },
    ],
    'no-console': [
      'warn',
      {
        allow: [
          'info',
          'error',
          'warn',
          'dir',
          'timeLog',
          'assert',
          'clear',
          'count',
          'countReset',
          'group',
          'groupEnd',
          'table',
          'dirxml',
          'groupCollapsed',
          'Console',
          'profile',
          'profileEnd',
          'timeStamp',
          'context',
        ],
      },
    ],
    'no-const-assign': 'error',
    'no-constant-condition': [
      'error',
      {
        checkLoops: true,
      },
    ],
    'no-constructor-return': 'warn',
    'no-continue': 'warn',
    'no-control-regex': 'warn',
    'no-debugger': 'error',
    'no-delete-var': 'warn',
    'no-div-regex': 'warn',
    'no-dupe-args': 'error',
    'no-dupe-class-members': 'warn',
    'no-dupe-else-if': 'error',
    'no-dupe-keys': 'error',
    'no-duplicate-case': 'error',
    'no-duplicate-imports': 'warn',
    'no-else-return': [
      'warn',
      {
        allowElseIf: true,
      },
    ],
    'no-empty-character-class': 'warn',
    'no-empty-function': 'off',
    'no-empty-pattern': 'warn',
    'no-empty': 'warn',
    'no-eq-null': 'warn',
    'no-eval': 'error',
    'no-ex-assign': 'error',
    'no-extend-native': 'warn',
    'no-extra-bind': 'warn',
    'no-extra-boolean-cast': [
      'error',
      {
        enforceForLogicalOperands: true,
      },
    ],
    'no-extra-label': 'warn',
    'no-extra-parens': [
      'error',
      'all',
      {
        ignoreJSX: 'multi-line',
        nestedBinaryExpressions: false,
        enforceForArrowConditionals: false,
      },
    ],
    'no-extra-semi': 'warn',
    'no-fallthrough': 'warn',
    'no-floating-decimal': 'warn',
    'no-func-assign': 'error',
    'no-global-assign': 'error',
    'no-implicit-coercion': 'warn',
    'no-implicit-globals': 'warn',
    'no-implied-eval': 'error',
    'no-import-assign': 'error',
    'no-inline-comments': 'off',
    'no-inner-declarations': 'off',
    'no-invalid-regexp': [
      'error',
      {
        allowConstructorFlags: ['u', 'y'],
      },
    ],
    'no-invalid-this': [
      'warn',
      {
        capIsConstructor: true,
      },
    ],
    'no-irregular-whitespace': [
      'warn',
      {
        skipStrings: false,
        skipComments: false,
        skipRegExps: false,
        skipTemplates: false,
      },
    ],
    'no-iterator': 'error',
    'no-label-var': 'error',
    'no-labels': 'error',
    'no-lone-blocks': 'warn',
    'no-lonely-if': 'warn',
    'no-loop-func': 'warn',
    'no-loss-of-precision': 'error',
    'no-magic-numbers': [
      'off', // turn off warnings for magic numbers because in frontend applications there are more non-magic numbers in styling properties than actual magic numbers. It would take a lot of disable/enable statements... just try to be mindfull of it and provide some constants when it seems like a good idea.
      // 'warn', {
      //   ignore: [
      //     0,
      //     1,
      //     60,
      //     3600,
      //   ],
      //   detectObjects: true,
      //   enforceConst: true,
      //   ignoreArrayIndexes: true,
      //   ignoreDefaultValues: false,
      // },
    ],
    'no-misleading-character-class': 'off',
    'no-mixed-operators': [
      'warn',
      {
        allowSamePrecedence: false,
      },
    ],
    'no-mixed-spaces-and-tabs': 'warn',
    'no-multi-assign': 'warn',
    'no-multi-spaces': [
      'warn',
      {
        ignoreEOLComments: true,
        exceptions: {
          ImportDeclaration: true,
          Property: true,
        },
      },
    ],
    'no-multi-str': 'warn',
    'no-multiple-empty-lines': [
      'warn',
      {
        max: 2,
      },
    ],
    'no-negated-condition': 'off', // Turned off to be able to write ternary expression based as "a !== undefined ? b : c"
    'no-nested-ternary': 'off', // Handled by unicorn/no-nested-ternary
    'no-new-func': 'error',
    'no-new-object': 'error',
    'no-new-symbol': 'error',
    'no-new-wrappers': 'error',
    'no-new': 'error',
    'no-nonoctal-decimal-escape': 'warn',
    'no-obj-calls': 'error',
    'no-octal-escape': 'error',
    'no-octal': 'error',
    'no-param-reassign': 'error',
    'no-plusplus': [
      'warn',
      {
        allowForLoopAfterthoughts: true,
      },
    ],
    'no-promise-executor-return': 'error',
    'no-proto': 'error',
    'no-prototype-builtins': 'error',
    'no-redeclare': [
      'error',
      {
        builtinGlobals: true,
      },
    ],
    'no-regex-spaces': 'warn',
    'no-restricted-exports': 'off',
    'no-restricted-globals': ['error'].concat(restrictedGlobals),
    'no-restricted-imports': [
      'error',
      'assert',
      'buffer',
      'child_process',
      'cluster',
      'crypto',
      'dgram',
      'dns',
      'domain',
      'events',
      'freelist',
      'fs',
      'http',
      'https',
      'module',
      'net',
      'os',
      'path',
      'punycode',
      'querystring',
      'readline',
      'repl',
      'smalloc',
      'stream',
      'string_decoder',
      'sys',
      'timers',
      'tls',
      'tracing',
      'tty',
      'url',
      'util',
      'vm',
      'zlib',
      // 'underscore',
      // 'lodash',
      // 'ramda',
      // 'moment',
      // 'date-fns',
    ],
    'no-restricted-properties': 'off',
    'no-restricted-syntax': 'off',
    'no-return-assign': ['error', 'except-parens'],
    'no-return-await': 'warn',
    'no-script-url': 'error',
    'no-self-assign': 'error',
    'no-self-compare': 'error',
    'no-sequences': 'warn',
    'no-setter-return': 'error',
    'no-shadow-restricted-names': 'error',
    'no-shadow': 'error',
    'no-sparse-arrays': 'warn',
    'no-tabs': 'warn',
    'no-template-curly-in-string': 'warn',
    'no-ternary': 'off',
    'no-this-before-super': 'error',
    'no-throw-literal': 'warn',
    'no-trailing-spaces': 'warn',
    'no-undef-init': 'warn',
    'no-undef': [
      'error',
      {
        typeof: true,
      },
    ],
    'no-undefined': 'off', // doesn't improve readability...
    'no-underscore-dangle': [
      'error',
      {
        allow: ['_page', '_link'],
        allowFunctionParams: true,
      },
    ],
    'no-unexpected-multiline': 'warn',
    'no-unmodified-loop-condition': 'warn',
    'no-unneeded-ternary': 'warn',
    'no-unreachable-loop': 'error',
    'no-unreachable': 'error',
    'no-unsafe-finally': 'error',
    'no-unsafe-negation': 'warn',
    'no-unsafe-optional-chaining': 'warn',
    'no-unused-expressions': [
      'error',
      {
        allowShortCircuit: true,
        allowTernary: true,
        allowTaggedTemplates: true,
      },
    ],
    'no-unused-labels': 'error',
    'no-unused-vars': [
      'warn',
      {
        caughtErrors: 'all',
        ignoreRestSiblings: true,
        vars: 'all',
        args: 'none',
      },
    ],
    'no-use-before-define': 'warn',
    'no-useless-backreference': 'warn',
    'no-useless-call': 'warn',
    'no-useless-catch': 'warn',
    'no-useless-computed-key': 'warn',
    'no-useless-concat': 'warn',
    'no-useless-constructor': 'warn',
    'no-useless-escape': 'warn',
    'no-var': 'warn',
    'no-void': [
      'warn',
      {
        allowAsStatement: true,
      },
    ],
    'no-warning-comments': 'off',
    'no-whitespace-before-property': 'error',
    'no-with': 'error',
    'nonblock-statement-body-position': ['warn', 'beside'],
    'object-curly-newline': [
      'warn',
      {
        ObjectExpression: {
          multiline: true,
          minProperties: 1,
          consistent: true,
        },
        ObjectPattern: {
          multiline: true,
          minProperties: 1,
          consistent: true,
        },
        ImportDeclaration: 'never',
        ExportDeclaration: 'always',
      },
    ],
    'object-curly-spacing': [
      'error',
      'always',
      {
        arraysInObjects: false,
        objectsInObjects: false,
      },
    ],
    'object-property-newline': [
      'warn',
      {
        allowAllPropertiesOnSameLine: false,
      },
    ], // Has no impact on import/export objects, see "modules-newlines/" rules for that below.
    'object-shorthand': [
      'warn',
      'always',
      {
        avoidExplicitReturnArrows: false,
      },
    ],
    'one-var-declaration-per-line': 'warn',
    'one-var': ['warn', 'never'],
    'operator-assignment': ['warn', 'always'],
    'operator-linebreak': [
      'warn',
      'before',
      {
        overrides: {
          '=': 'ignore',
        },
      },
    ],
    'padded-blocks': ['warn', 'never'],
    'prefer-arrow-callback': [
      'warn',
      {
        allowNamedFunctions: true,
      },
    ],
    // 'prefer-const': 'warn', // Behaves annoying during development when it automatically changes "let"s to "const"s before you had the chance to (re)assign a value.
    'prefer-destructuring': [
      'warn',
      {
        VariableDeclarator: {
          array: false,
          object: true,
        },
        AssignmentExpression: {
          array: true,
          object: true,
        },
      },
      {
        enforceForRenamedProperties: false,
      },
    ],
    'prefer-exponentiation-operator': 'off',
    'prefer-named-capture-group': 'warn',
    'prefer-numeric-literals': 'off',
    'prefer-object-spread': 'warn',
    'prefer-promise-reject-errors': [
      'warn',
      {
        allowEmptyReject: true,
      },
    ],
    'prefer-regex-literals': 'warn',
    'prefer-rest-params': 'warn',
    'prefer-spread': 'warn',
    'prefer-template': 'warn',
    'quote-props': ['warn', 'consistent-as-needed'],
    'quotes': [
      'warn',
      'single',
      {
        avoidEscape: true,
        allowTemplateLiterals: true,
      },
    ],
    'radix': ['warn', 'as-needed'],
    'require-atomic-updates': 'warn',
    'require-await': 'warn',
    'require-unicode-regexp': 'warn',
    'require-yield': 'warn',
    'rest-spread-spacing': ['warn', 'never'],
    'semi-spacing': [
      'warn',
      {
        before: false,
        after: true,
      },
    ],
    'semi-style': ['warn', 'last'],
    'semi': ['warn', 'never'],
    'sort-imports': [
      'warn',
      {
        ignoreCase: true,
        ignoreDeclarationSort: true, // Let the declaration sort be handled by eslint-plugin-import/order
        ignoreMemberSort: true, // Enable sort within a multiple member import declaration e.g. "{a, b, c} from"
        allowSeparatedGroups: true, // Check the sorting of import declaration statements only for those that appear on consecutive lines
      },
    ],
    // 'sort-keys': [
    //   'warn',
    //   'asc',
    //   { caseSensitive: false, natural: true },
    // ],
    // ^ Unfortunately there is no autofix available which makes it a bit annoying.
    // As an alternative, we can use vscode's "sort lines ascending" functionality on a selection of object keys, which comes close to autofixing (pro-tip: map this to a shortcut similar to the "format" shortcut).
    // And/or we found another eslint plugin, which _can_ fix it! (See further below)
    'sort-vars': [
      'warn',
      {
        ignoreCase: true,
      },
    ],
    'space-before-blocks': ['warn', 'always'],
    'space-before-function-paren': [
      'warn',
      {
        anonymous: 'never',
        named: 'never',
        asyncArrow: 'always',
      },
    ],
    'space-in-parens': ['warn', 'never'],
    'space-infix-ops': 'warn',
    'space-unary-ops': [
      'warn',
      {
        words: true,
        nonwords: false,
      },
    ],
    'spaced-comment': [
      'warn',
      'always',
      {
        markers: ['/'],
      },
    ],
    'strict': ['warn', 'safe'],
    'switch-colon-spacing': [
      'warn',
      {
        after: true,
        before: false,
      },
    ],
    'symbol-description': 'warn',
    'template-curly-spacing': ['warn', 'never'],
    'template-tag-spacing': ['warn', 'never'],
    'unicode-bom': ['warn', 'never'],
    'use-isnan': [
      'error',
      {
        enforceForIndexOf: true,
        enforceForSwitchCase: true,
      },
    ],
    'valid-typeof': [
      'error',
      {
        requireStringLiterals: true,
      },
    ],
    'vars-on-top': 'off',
    'wrap-iife': ['error', 'outside'],
    'wrap-regex': 'off',
    'yield-star-spacing': [
      'warn',
      {
        before: true,
        after: false,
      },
    ],
    'yoda': 'warn', // Require or disallow Yoda conditions

    /* ESLINT-PLUGIN-ESLINT-COMMENTS RULES (https://mysticatea.github.io/eslint-plugin-eslint-comments/) */
    'eslint-comments/disable-enable-pair': [
      'warn',
      {
        allowWholeFile: true,
      },
    ], // Allow disabling rules for the entire file while still catching "open" eslint-disable directives in the middle of a file.
    'eslint-comments/no-aggregating-enable': 'warn', // Disallows eslint-enable comments for multiple eslint-disable comments
    'eslint-comments/no-duplicate-disable': 'warn', // Disallows duplicate eslint-disable comments
    'eslint-comments/no-unlimited-disable': 'error', // Disallows eslint-disable comments without rule names
    'eslint-comments/no-unused-disable': 'warn', // Disallow disables that don't cover any errors
    'eslint-comments/no-unused-enable': 'warn', // Disallow enables that don't enable anything or enable rules that weren't disabled
    'eslint-comments/require-description': [ // Disallow eslint-disable comments without descriptions so that others can understand and/or learn why eslint was enabled/disabled
      'warn',
      {
        ignore: [
          'eslint',
          // 'eslint-disable', // needs a description
          // 'eslint-disable-line', // needs a description
          // 'eslint-disable-next-line', // needs a description
          'eslint-enable',
          'eslint-env',
          'exported',
          'global',
          'globals',
        ],
      },
    ],

    /* IMPORT RULES */
    'import/default': 'error',
    'import/dynamic-import-chunkname': 'error', // When you don't explicitly name chunks, webpack will autogenerate chunk names that are not consistent across builds, which prevents long-term browser caching.
    'import/export': 'error',
    'import/exports-last': 'off', // Creates a conflict with exported variables which are, perhaps rightfully, declared above other non-exported variables. It also doesn't do much regarding code quality, it even worsens development speed as you have to write extra lines of code to export at the end.
    'import/extensions': [
      'warn',
      'never',
      {
        jpeg: 'always',
        jpg: 'always',
        png: 'always',
        svg: 'always',
        json: 'always',
      },
    ],
    'import/first': 'warn',
    'import/group-exports': 'off', // Only useful when creating a library or util, and it would mean too much confusion in other files containing actual application code or too much enabling/disabling between files. It also doesn't do much regarding code quality, it even worsens development speed as you have to write extra lines of code to bundle exports.
    'import/max-dependencies': 'off',
    'import/named': 'error',
    'import/namespace': 'error',
    'import/newline-after-import': [
      'warn',
      {
        count: 2,
      },
    ],
    'import/no-absolute-path': 'warn',
    'import/no-amd': 'warn',
    'import/no-anonymous-default-export': [
      'error',
      {
        allowArray: false,
        allowArrowFunction: false,
        allowAnonymousClass: false,
        allowAnonymousFunction: false,
        allowCallExpression: true, // The true value here is for backward compatibility
        allowLiteral: true,
        allowObject: true,
      },
    ],
    'import/no-commonjs': 'warn',
    'import/no-cycle': [
      'error',
      {
        ignoreExternal: true,
        maxDepth: 2,
      },
    ],
    'import/no-default-export': 'off',
    'import/no-deprecated': 'warn',
    'import/no-duplicates': 'off', // Already handled by eslint's 'no-duplicate-imports'
    'import/no-dynamic-require': 'warn',
    'import/no-extraneous-dependencies': [
      'error',
      {
        devDependencies: false,
        optionalDependencies: false,
        peerDependencies: false,
        bundledDependencies: true,
      },
    ],
    'import/no-internal-modules': 'off', // can be useful when path mapping is used (https://www.typescriptlang.org/docs/handbook/module-resolution.html#path-mapping)
    'import/no-mutable-exports': 'warn',
    'import/no-named-as-default-member': 'warn',
    'import/no-named-as-default': 'warn',
    'import/no-named-export': 'off',
    'import/no-namespace': 'off', // Turned off so that ACPaaS UI library can be imported as a namespace (AUI)
    'import/no-nodejs-modules': 'off',
    'import/no-relative-parent-imports': 'off',
    'import/no-restricted-paths': 'off', // Can be useful for monorepos.
    'import/no-self-import': 'error',
    'import/no-unassigned-import': 'off', // Too much enabling/disabling between files
    'import/no-unresolved': 'error',
    'import/no-unused-modules': 'warn', // Let's see what happens
    'import/no-useless-path-segments': [
      'warn',
      {
        noUselessIndex: true,
      },
    ],
    'import/no-webpack-loader-syntax': 'error',
    'import/order': [
      'warn',
      {
        'alphabetize': {
          caseInsensitive: true, // ignore case. Options: [true, false]
          order: 'asc', // sort in ascending order. Options: ['ignore', 'asc', 'desc']
        },
        'newlines-between': 'always', // at least one new line between each group will be enforced, and new lines inside a group will be forbidden
      },
    ],
    'import/prefer-default-export': 'off', // turned off, try to use common sense when exporting: prefer default export for "a react component", "a service", "a module", ...  pretty much anything which can be viewed as the _only_ thing being exported in a file. Other things like helpers, utils, constants, can be non-default as it is more likely those files will have more exports.
    'import/unambiguous': 'off',

    /* JEST RULES */
    'jest/consistent-test-it': 'warn',
    'jest/expect-expect': 'warn',
    'jest/lowercase-name': [
      'warn',
      {
        ignoreTopLevelDescribe: true,
      },
    ],
    'jest/no-alias-methods': 'warn',
    'jest/no-commented-out-tests': 'off',
    'jest/no-conditional-expect': 'warn',
    'jest/no-deprecated-functions': 'error',
    'jest/no-disabled-tests': 'warn',
    'jest/no-done-callback': 'warn',
    'jest/no-duplicate-hooks': 'error',
    'jest/no-export': 'warn',
    'jest/no-focused-tests': 'warn',
    'jest/no-hooks': 'off',
    'jest/no-identical-title': 'warn',
    'jest/no-if': 'warn',
    'jest/no-interpolation-in-snapshots': 'warn',
    'jest/no-jasmine-globals': 'error',
    'jest/no-jest-import': 'error',
    'jest/no-large-snapshots': 'off',
    'jest/no-mocks-import': 'warn',
    'jest/no-restricted-matchers': [
      'error',
      {
        toBeTruthy: 'Avoid `.toBeTruthy` and use `.toBe(true)` instead',
        toBeFalsy: 'Avoid `.toBeFalsy` and use `.toBe(false)` instead',
      },
    ],
    'jest/no-standalone-expect': 'warn',
    'jest/no-test-prefixes': 'warn',
    'jest/no-test-return-statement': 'error',
    'jest/prefer-called-with': 'warn',
    'jest/prefer-expect-assertions': 'off',
    'jest/prefer-hooks-on-top': 'warn',
    'jest/prefer-spy-on': 'warn',
    'jest/prefer-strict-equal': 'warn',
    'jest/prefer-to-be-null': 'warn',
    'jest/prefer-to-be-undefined': 'warn',
    'jest/prefer-to-contain': 'warn',
    'jest/prefer-to-have-length': 'warn',
    'jest/prefer-todo': 'warn',
    'jest/require-to-throw-message': 'warn',
    'jest/require-top-level-describe': 'warn',
    'jest/valid-describe': 'warn',
    'jest/valid-expect-in-promise': 'warn',
    'jest/valid-expect': 'warn',
    'jest/valid-title': 'warn',

    /* JSX-A11Y RULES */
    'jsx-a11y/accessible-emoji': 'warn',
    'jsx-a11y/alt-text': 'warn',
    'jsx-a11y/anchor-has-content': 'warn',
    'jsx-a11y/anchor-is-valid': [
      'warn',
      {
        aspects: [
          'noHref',
          'invalidHref',
          'preferButton',
        ],
        components: ['Link'],
      },
    ],
    'jsx-a11y/aria-activedescendant-has-tabindex': 'warn',
    'jsx-a11y/aria-props': 'error',
    'jsx-a11y/aria-proptypes': 'error',
    'jsx-a11y/aria-role': [
      'warn',
      {
        ignoreNonDOM: true,
      },
    ],
    'jsx-a11y/aria-unsupported-elements': 'error',
    'jsx-a11y/autocomplete-valid': [
      'warn',
      {
        inputComponents: ['CustomInput'], // Add custom fields/components when necessary
      },
    ],
    'jsx-a11y/click-events-have-key-events': 'warn',
    'jsx-a11y/control-has-associated-label': [
      'off',
      {
        ignoreElements: [
          'audio',
          'canvas',
          'embed',
          'input',
          'textarea',
          'tr',
          'video',
        ],
        ignoreRoles: [
          'grid',
          'listbox',
          'menu',
          'menubar',
          'radiogroup',
          'row',
          'tablist',
          'toolbar',
          'tree',
          'treegrid',
        ],
        includeRoles: ['alert', 'dialog'],
      },
    ],
    'jsx-a11y/heading-has-content': 'warn',
    'jsx-a11y/html-has-lang': 'warn',
    'jsx-a11y/iframe-has-title': 'warn',
    'jsx-a11y/img-redundant-alt': [
      'warn',
      {
        components: ['Image'],
        words: ['Beeld', 'Foto'],
      },
    ],
    'jsx-a11y/interactive-supports-focus': [
      'warn',
      {
        tabbable: [
          'button',
          'checkbox',
          'combobox',
          'link',
          'menu',
          'menubar',
          'menuitem',
          'menuitemcheckbox',
          'menuitemradio',
          'option',
          'progressbar',
          'radio',
          'searchbox',
          'slider',
          'spinbutton',
          'switch',
          'tab',
          'textbox',
        ],
      },
    ],
    'jsx-a11y/label-has-for': 'off',
    'jsx-a11y/label-has-associated-control': [
      'warn',
      { // Adjust to the correct component(s) and property(/ies) when necessary
        labelComponents: ['CustomLabel'],
        labelAttributes: ['inputLabel'],
        controlComponents: ['CustomInput'],
        assert: 'both',
        depth: 3,
      },
    ],
    'jsx-a11y/lang': 'warn',
    'jsx-a11y/media-has-caption': [
      'warn',
      { // Adjust to the correct component(s) and property(/ies) when necessary
        audio: ['Audio'],
        video: ['Video'],
        track: ['Track'],
      },
    ],
    'jsx-a11y/mouse-events-have-key-events': 'warn',
    'jsx-a11y/no-access-key': 'error',
    'jsx-a11y/no-autofocus': [
      'error',
      {
        ignoreNonDOM: false,
      },
    ],
    'jsx-a11y/no-distracting-elements': 'warn',
    'jsx-a11y/no-interactive-element-to-noninteractive-role': 'warn',
    'jsx-a11y/no-noninteractive-element-interactions': [
      'error',
      {
        handlers: [
          'onClick',
          'onMouseDown',
          'onMouseUp',
          'onKeyPress',
          'onKeyDown',
          'onKeyUp',
        ],
        body: ['onError', 'onLoad'],
        iframe: ['onError', 'onLoad'],
        img: ['onError', 'onLoad'],
      },
    ],
    'jsx-a11y/no-noninteractive-element-to-interactive-role': [
      'warn',
      {
        ul: [
          'listbox',
          'menu',
          'menubar',
          'radiogroup',
          'tablist',
          'tree',
          'treegrid',
        ],
        ol: [
          'listbox',
          'menu',
          'menubar',
          'radiogroup',
          'tablist',
          'tree',
          'treegrid',
        ],
        li: [
          'menuitem',
          'option',
          'row',
          'tab',
          'treeitem',
        ],
        table: ['grid'],
        td: ['gridcell'],
      },
    ],
    'jsx-a11y/no-noninteractive-tabindex': [
      'warn',
      {
        tags: [],
        roles: ['tabpanel'],
      },
    ],
    'jsx-a11y/no-redundant-roles': [
      'warn',
      {
        nav: ['navigation'],
      },
    ],
    'jsx-a11y/no-static-element-interactions': [
      'warn',
      {
        handlers: [
          'onClick',
          'onMouseDown',
          'onMouseUp',
          'onKeyPress',
          'onKeyDown',
          'onKeyUp',
        ],
      },
    ],
    'jsx-a11y/role-has-required-aria-props': 'warn',
    'jsx-a11y/role-supports-aria-props': 'error',
    'jsx-a11y/scope': 'error',
    'jsx-a11y/tabindex-no-positive': 'warn',

    /* MODULES-NEWLINE RULES */
    'modules-newline/import-declaration-newline': 'warn',
    'modules-newline/export-declaration-newline': 'warn',

    /* PROMISE RULES */
    'promise/always-return': 'warn', // not because we "always need to return", but to help in those cases it is actually necessary. And if it's not necessary, just put in a comment like "// eslint-disable-next-line promise/always-return -- no need to return a value"
    'promise/avoid-new': 'off',
    'promise/catch-or-return': 'error',
    'promise/no-callback-in-promise': 'warn', // Let's see what happens
    'promise/no-native': 'off', // we're in an ES6 environment
    'promise/no-nesting': 'warn',
    'promise/no-new-statics': 'off',
    'promise/no-promise-in-callback': 'off',
    'promise/no-return-in-finally': 'error',
    'promise/no-return-wrap': 'warn',
    'promise/param-names': 'error',
    'promise/prefer-await-to-callbacks': 'warn',
    'promise/prefer-await-to-then': 'off', // disabled because sometimes it is better to use async/await, just try to be consistent and mindful about it
    'promise/valid-params': 'warn',

    /* REACT RULES */
    'react/boolean-prop-naming': [
      'error',
      {
        message: 'It is better if your boolean prop ({{ propName }}) starts with \'is\', \'has\', \'can\', \'should\' to make it obvious to developers that it is a boolean property. Regex pattern to match: ({{ pattern }})',
        rule: '^(is|has|can|should)[A-Z]([A-Za-z0-9]?)+',
        validateNested: true,
      },
    ],
    'react/button-has-type': 'warn',
    'react/default-props-match-prop-types': 'warn',
    'react/destructuring-assignment': [
      'warn',
      'always',
      {
        ignoreClassFields: true,
      },
    ],
    'react/display-name': 'off', // can be useful if you really need to find an element in React debugging messages
    'react/forbid-component-props': [
      'off', // 'warn', // turned off because it is not used at the moment
      {
        forbid: [
          // 'className', // allow the 'className' property as external libraries use this too often
          // 'style' // allow the 'style' property as it is necessary to be able to style React Native components
        ],
      },
    ],
    'react/forbid-dom-props': 'off', // no idea why this would ever be useful
    'react/forbid-elements': 'off', // can be useful, for example, to disable usage of basic html 'form' elements and force formiks' Form components
    'react/forbid-foreign-prop-types': 'warn',
    'react/forbid-prop-types': 'warn',
    'react/function-component-definition': [
      'warn',
      {
        namedComponents: 'arrow-function',
        unnamedComponents: 'arrow-function',
      },
    ],
    'react/no-access-state-in-setstate': 'error',
    'react/no-adjacent-inline-elements': 'warn', // Adjacent inline elements not separated by whitespace will bump up against each other when viewed in an unstyled manner, which usually isn't desirable.
    'react/no-array-index-key': 'error', // React became better at preventing this bug but still it's a good idea to use uid's instead of index as the key of an element in a render-loop
    'react/no-children-prop': 'error',
    'react/no-danger': 'warn',
    'react/no-danger-with-children': 'warn',
    'react/no-deprecated': 'error',
    'react/no-did-mount-set-state': ['error', 'disallow-in-func'],
    'react/no-did-update-set-state': 'warn',
    'react/no-direct-mutation-state': 'error',
    'react/no-find-dom-node': 'error',
    'react/no-is-mounted': 'error',
    'react/no-multi-comp': [
      'warn',
      {
        ignoreStateless: true,
      },
    ],
    'react/no-redundant-should-component-update': 'warn',
    'react/no-render-return-value': 'warn',
    'react/no-set-state': 'off',
    'react/no-string-refs': [
      'error',
      {
        noTemplateLiterals: true,
      },
    ], // https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/no-string-refs.md & https://reactjs.org/docs/handling-events.html & https://reactjs.org/docs/refs-and-the-dom.html#legacy-api-string-refs
    'react/no-this-in-sfc': 'error',
    'react/no-typos': 'warn',
    'react/no-unescaped-entities': 'warn',
    'react/no-unknown-property': 'warn',
    'react/no-unsafe': 'error',
    'react/no-unused-prop-types': 'warn',
    'react/no-unused-state': 'warn',
    'react/no-will-update-set-state': ['error', 'disallow-in-func'],
    'react/prefer-es6-class': ['warn', 'always'],
    'react/prefer-read-only-props': 'off',
    'react/prefer-stateless-function': [
      'warn',
      {
        ignorePureComponents: true,
      },
    ],
    'react/prop-types': 'off', // https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/prop-types.md - Turned off because it gives warnings even though the property types are described by defining the type of a component using "React.FC<MyPropTypes>". Also, by using TypeScript and not allowing the "any" type we can check property types during compilation, and that's even better as the 'prop-types' are only checked during runtime.
    'react/react-in-jsx-scope': 'off', // No longer necessary with React v17
    'react/require-default-props': [
      'warn',
      {
        forbidDefaultForRequired: true,
        ignoreFunctionalComponents: false,
      },
    ],
    'react/require-optimization': 'off',
    'react/require-render-return': 'error',
    'react/self-closing-comp': 'error',
    'react/sort-comp': [
      'warn',
      {
        groups: {
          lifecycle: [
            'displayName',
            'propTypes',
            'contextTypes',
            'childContextTypes',
            'mixins',
            'statics',
            'defaultProps',
            'constructor',
            'getDefaultProps',
            'state',
            'getInitialState',
            'getChildContext',
            'getDerivedStateFromProps',
            'componentWillMount',
            'UNSAFE_componentWillMount',
            'componentDidMount',
            'componentWillReceiveProps',
            'UNSAFE_componentWillReceiveProps',
            'shouldComponentUpdate',
            'componentWillUpdate',
            'UNSAFE_componentWillUpdate',
            'getSnapshotBeforeUpdate',
            'componentDidUpdate',
            'componentDidCatch',
            'componentWillUnmount',
          ],
        },
        order: [
          'static-methods',
          'lifecycle',
          'render',
          '/^on.+$/',
          'everything-else',
        ],
      },
    ],
    'react/sort-prop-types': [
      'warn',
      {
        ignoreCase: true,
        requiredFirst: true,
        sortShapeProp: true,
      },
    ],
    'react/state-in-constructor': ['warn', 'always'],
    'react/static-property-placement': 'warn',
    'react/style-prop-object': 'error',
    'react/void-dom-elements-no-children': 'error',
    // react/jsx specific rules
    'react/jsx-boolean-value': [
      'error',
      'always',
      {
        never: [],
      },
    ],
    'react/jsx-child-element-spacing': 'warn',
    'react/jsx-closing-bracket-location': [
      'warn',
      {
        nonEmpty: 'tag-aligned',
        selfClosing: 'tag-aligned', // or 'after-props'
      },
    ],
    'react/jsx-closing-tag-location': 'warn',
    'react/jsx-curly-brace-presence': ['warn', 'never'],
    'react/jsx-curly-newline': [
      'warn',
      {
        multiline: 'require',
        singleline: 'consistent',
      },
    ],
    'react/jsx-curly-spacing': [
      'warn',
      {
        when: 'never',
        attributes: {
          allowMultiline: true,
        },
        children: true,
      },
    ],
    'react/jsx-equals-spacing': ['warn', 'never'],
    'react/jsx-filename-extension': 'off',
    'react/jsx-first-prop-new-line': ['warn', 'multiline'], // 'multiline' makes sure attributes containing multiline objects are placed on a newline, which in turn enforces proper indentation through eslint (with no newline it would otherwise indent the object aligned to the jsx opening tag).
    'react/jsx-fragments': ['warn', 'element'],
    'react/jsx-handler-names': [
      'warn',
      {
        eventHandlerPrefix: '(handle|set|formikProps.handle|properties.on)', // Functions as a regex (docs don't mention this, see (https://github.com/yannickcr/eslint-plugin-react/issues/1687#issuecomment-385208376): allow 'set' so that hook callbacks (which start with 'set...' by default) can easily be used as well. Also, allow 'formikProps.handle...' as preparation for when Formik lib is used. Also, allows 'properties.on...' to allow prop-drilling.
        eventHandlerPropPrefix: 'on',
        checkLocalVariables: true,
        checkInlineFunction: true,
      },
    ],
    'react/jsx-indent': [
      'warn',
      2, // = 2 spaces, would be nice to set it to 'first' or something like that so that it would align values of attributes to their property declaration
      {
        checkAttributes: false, // checks indention of values within jsx attributes, best to set it to false because it hardly works
        indentLogicalExpressions: true,
      },
    ],
    'react/jsx-indent-props': ['warn', 2], // 'first' works well in combination with 'jsx-first-prop-no-newline' set to 'never' but that breaks indentation if the first attribute contains a multiline object.
    'react/jsx-key': [
      'error',
      {
        checkFragmentShorthand: true,
        checkKeyMustBeforeSpread: true,
      },
    ],
    'react/jsx-max-depth': 'off', // too random
    'react/jsx-max-props-per-line': [
      'warn',
      {
        maximum: 1,
        when: 'always',
      },
    ],
    'react/jsx-newline': 'off',
    'react/jsx-no-bind': [
      'error',
      {
        ignoreDOMComponents: true, // https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/jsx-no-bind.md#ignoredomcomponents & https://reactjs.org/docs/handling-events.html - You can't mess up performance as it can not be propdrilled any further
        allowArrowFunctions: true,
      },
    ],
    'react/jsx-no-comment-textnodes': 'error',
    'react/jsx-no-constructed-context-values': 'error',
    'react/jsx-no-duplicate-props': 'error',
    'react/jsx-no-literals': 'off', // can be (ab)used to ensure strings are part of a translation system
    'react/jsx-no-script-url': 'error',
    'react/jsx-no-target-blank': [
      'error',
      {
        allowReferrer: false,
        enforceDynamicLinks: 'always',
        warnOnSpreadAttributes: true,
      },
    ],
    'react/jsx-no-undef': 'error',
    'react/jsx-no-useless-fragment': 'warn',
    'react/jsx-one-expression-per-line': [
      'warn',
      {
        allow: 'single-child',
      },
    ],
    'react/jsx-pascal-case': 'error',
    'react/jsx-props-no-multi-spaces': 'warn', // should already be handled by 'no-multi-spaces' but let's see what happens
    'react/jsx-props-no-spreading': 'warn',
    'react/jsx-sort-default-props': [
      'warn',
      {
        ignoreCase: true,
      },
    ], // https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/jsx-sort-default-props.md
    'react/jsx-sort-props': [
      // https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/jsx-sort-props.md
      'warn',
      {
        ignoreCase: true,
        reservedFirst: true,
      },
    ],
    'react/jsx-tag-spacing': [
      'warn',
      {
        closingSlash: 'never',
        beforeSelfClosing: 'always',
        afterOpening: 'never',
        beforeClosing: 'never',
      },
    ],
    'react/jsx-uses-react': 'off', // https://reactjs.org/blog/2020/09/22/introducing-the-new-jsx-transform.html#eslint
    'react/jsx-uses-vars': 'warn',
    'react/jsx-wrap-multilines': [
      'warn',
      {
        declaration: 'parens-new-line',
        assignment: 'parens-new-line',
        return: 'parens-new-line',
        arrow: 'parens-new-line',
        condition: 'parens-new-line',
        logical: 'parens-new-line',
        prop: 'parens-new-line',
      },
    ],

    /* REACT-HOOKS RULES */
    'react-hooks/rules-of-hooks': 'error',
    'react-hooks/exhaustive-deps': 'error',

    /* SORT-KEYS-FIX RULES */
    'sort-keys-fix/sort-keys-fix': [
      'warn',
      'asc',
      {
        caseSensitive: false,
        natural: true,
      },
    ],

    /* UNICORN RULES */
    'unicorn/better-regex': 'warn', // Improve regexes by making them shorter, consistent, and safer. (fixable)
    'unicorn/catch-error-name': 'warn', // Enforce a specific parameter name in catch clauses. (fixable)
    'unicorn/consistent-destructuring': 'warn', // Use destructured variables over properties. (partly fixable)
    'unicorn/consistent-function-scoping': 'warn', // Move function definitions to the highest possible scope.
    'unicorn/custom-error-definition': 'warn', // Enforce correct Error subclassing. (fixable)
    'unicorn/empty-brace-spaces': 'warn', // Enforce no spaces between braces. (fixable)
    'unicorn/error-message': 'warn', // Enforce passing a message value when creating a built-in error.
    'unicorn/escape-case': 'warn', // Require escape sequences to use uppercase values. (fixable)
    'unicorn/expiring-todo-comments': 'off', // Add expiration conditions to TODO comments.
    'unicorn/explicit-length-check': [ // Enforce explicitly comparing the length property of a value. (partly fixable)
      'error',
      {
        'non-zero': 'greater-than',
      },
    ],
    'unicorn/filename-case': 'off', // Enforce a case style for filenames.
    'unicorn/import-index': 'off', // Enforce importing index files with .. (fixable)
    'unicorn/import-style': 'off', // Enforce specific import styles per module.
    'unicorn/new-for-builtins': 'warn', // Enforce the use of new for all builtins, except String, Number, Boolean, Symbol and BigInt. (partly fixable)
    'unicorn/no-abusive-eslint-disable': 'off', // Enforce specifying rules to disable in eslint-disable comments.
    'unicorn/no-array-callback-reference': 'warn', // Prevent passing a function reference directly to iterator methods.
    // 'unicorn/no-array-push-push': 'warn', // Enforce combining multiple Array#push() into one call. (partly fixable) -- Not available for some reason.
    'unicorn/no-array-reduce': 'warn', // Disallow Array#reduce() and Array#reduceRight().
    'unicorn/no-console-spaces': 'warn', // Do not use leading/trailing space between console.log parameters. (fixable)
    'unicorn/no-for-loop': 'warn', // Do not use a for loop that can be replaced with a for-of loop. (partly fixable)
    'unicorn/no-hex-escape': 'warn', // Enforce the use of Unicode escapes instead of hexadecimal escapes. (fixable)
    'unicorn/no-instanceof-array': 'warn', // Require Array.isArray() instead of instanceof Array. (fixable)
    'unicorn/no-keyword-prefix': 'off', // Disallow identifiers starting with new or class. // Turned off because it's giving more false positives than actually helping
    'unicorn/no-lonely-if': 'warn', // Disallow if statements as the only statement in if blocks without else. (fixable)
    'unicorn/no-nested-ternary': 'warn', // Disallow nested ternary expressions. (partly fixable)
    'unicorn/no-new-array': 'warn', // Disallow new Array(). (partly fixable)
    'unicorn/no-new-buffer': 'warn', // Enforce the use of Buffer.from() and Buffer.alloc() instead of the deprecated new Buffer(). (fixable)
    'unicorn/no-null': [
      'warn',
      {
        checkStrictEquality: true,
      },
    ], // Disallow the use of the null literal.
    'unicorn/no-object-as-default-parameter': 'warn', // Disallow the use of objects as default parameters.
    'unicorn/no-process-exit': 'warn', // Disallow process.exit().
    'unicorn/no-unreadable-array-destructuring': 'warn', // Disallow unreadable array destructuring. (partly fixable)
    'unicorn/no-unsafe-regex': 'warn', // Disallow unsafe regular expressions.
    'unicorn/no-unused-properties': 'warn', // Disallow unused object properties.
    'unicorn/no-useless-undefined': 'off', // Turned off to allow 'return undefined', as it can make code more verbose.
    'unicorn/no-zero-fractions': 'warn', // Disallow number literals with zero fractions or dangling dots. (fixable)
    'unicorn/number-literal-case': 'warn', // Enforce proper case for numeric literals. (fixable)
    'unicorn/numeric-separators-style': 'warn', // Enforce the style of numeric separators by correctly grouping digits. (fixable)
    'unicorn/prefer-add-event-listener': 'warn', // Prefer .addEventListener() and .removeEventListener() over on-functions. (partly fixable)
    'unicorn/prefer-array-find': 'warn', // Prefer .find(…) over the first element from .filter(…). (partly fixable)
    'unicorn/prefer-array-flat-map': 'warn', // Prefer .flatMap(…) over .map(…).flat(). (fixable)
    'unicorn/prefer-array-index-of': 'warn', // Prefer Array#indexOf() over Array#findIndex() when looking for the index of an item. (partly fixable)
    'unicorn/prefer-array-some': 'warn', // Prefer .some(…) over .find(…).
    'unicorn/prefer-date-now': 'warn', // Prefer Date.now() to get the number of milliseconds since the Unix Epoch. (fixable)
    'unicorn/prefer-default-parameters': 'warn', // Prefer default parameters over reassignment. (fixable)
    'unicorn/prefer-dom-node-append': 'warn', // Prefer Node#append() over Node#appendChild(). (fixable)
    'unicorn/prefer-dom-node-dataset': 'warn', // Prefer using .dataset on DOM elements over .setAttribute(…). (fixable)
    'unicorn/prefer-dom-node-remove': 'warn', // Prefer childNode.remove() over parentNode.removeChild(childNode). (fixable)
    'unicorn/prefer-dom-node-text-content': 'warn', // Prefer .textContent over .innerText. (fixable)
    'unicorn/prefer-includes': 'warn', // Prefer .includes() over .indexOf() when checking for existence or non-existence. (fixable)
    'unicorn/prefer-keyboard-event-key': 'warn', // Prefer KeyboardEvent#key over KeyboardEvent#keyCode. (partly fixable)
    'unicorn/prefer-math-trunc': 'warn', // Enforce the use of Math.trunc instead of bitwise operators. (partly fixable)
    'unicorn/prefer-modern-dom-apis': 'warn', // Prefer .before() over .insertBefore(), .replaceWith() over .replaceChild(), prefer one of .before(), .after(), .append() or .prepend() over insertAdjacentText() and insertAdjacentElement(). (fixable)
    'unicorn/prefer-negative-index': 'warn', // index for {String,Array,TypedArray}#slice() and Array#splice(). (fixable)
    'unicorn/prefer-number-properties': 'warn', // Prefer Number static properties over global ones. (fixable)
    'unicorn/prefer-optional-catch-binding': 'off', // Prefer omitting the catch binding parameter. (fixable)
    'unicorn/prefer-query-selector': 'warn', // Prefer .querySelector() over .getElementById(), .querySelectorAll() over .getElementsByClassName() and .getElementsByTagName(). (partly fixable)
    'unicorn/prefer-reflect-apply': 'warn', // Prefer Reflect.apply() over Function#apply(). (fixable)
    'unicorn/prefer-regexp-test': 'warn', // Prefer RegExp#test() over String#match() and RegExp#exec(). (fixable)
    'unicorn/prefer-set-has': 'warn', // Prefer Set#has() over Array#includes() when checking for existence or non-existence. (fixable)
    'unicorn/prefer-spread': 'warn', // Prefer the spread operator over Array.from(). (fixable)
    'unicorn/prefer-string-replace-all': 'warn', // Prefer String#replaceAll() over regex searches with the global flag. (fixable)
    'unicorn/prefer-string-slice': 'warn', // Prefer String#slice() over String#substr() and String#substring(). (partly fixable)
    'unicorn/prefer-string-starts-ends-with': 'warn', // Prefer String#startsWith() & String#endsWith() over RegExp#test(). (fixable)
    'unicorn/prefer-string-trim-start-end': 'warn', // Prefer String#trimStart() / String#trimEnd() over String#trimLeft() / String#trimRight(). (fixable)
    'unicorn/prefer-ternary': 'warn', // Prefer ternary expressions over simple if-else statements. (fixable)
    'unicorn/prefer-type-error': 'warn', // Enforce throwing TypeError in type checking conditions. (fixable)
    'unicorn/prevent-abbreviations': [
      'warn',
      {
        replacements: {
          props: {
            properties: false,
          },
          args: {
            arguments: false,
          },
          res: {
            response: true,
            result: true,
            response_: false,
          },
          env: {
            environment: false,
          },
        },
      },
    ], // Prevent abbreviations. (partly fixable)
    'unicorn/string-content': 'warn', // Enforce better string content. (fixable)
    'unicorn/throw-new-error': 'warn', // Require new when throwing an error. (fixable)
  },
  settings: {
    'react': {
      version: 'detect',
    },
    'import/ignore': ['node_modules'],
    'linkComponents': [
      // Components used as alternatives to <a> for linking, eg. <Link to={ url } />
      {
        name: 'Link',
        linkAttribute: 'href',
      },
    ],
  },
}
