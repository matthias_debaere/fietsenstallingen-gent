# Fietsenstallingen GEnt

This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

- [Fietsenstallingen GEnt](#fietsenstallingen-gent)
  - [Getting Started](#getting-started)
    - [Environment Variables](#environment-variables)
    - [Running the project](#running-the-project)
  - [Tests](#tests)

## Getting Started

### Environment Variables

Create a _.env.local_ file with the following variable a valid values:

| Name                                | Value                                      |
| ----------------------------------- | ------------------------------------------ |
| **NEXT_PUBLIC_MAPBOX_ACCESS_TOKEN** | The access token to make queries to mapbox |

### Running the project

First, install the dependencies:

```bash
yarn install
```

Then, run the development server:

```bash
yarn dev
```

Finally, open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## Tests

```bash
# unit tests
$ yarn run test

# run tests in watchmode
$ yarn run test:watch

# test coverage
$ yarn run test:cov
```
