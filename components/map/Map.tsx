import { AxiosError } from 'axios'
import mapboxgl from 'mapbox-gl'
import React,
{ useRef,
  useEffect,
  useState } from 'react'
import 'mapbox-gl/dist/mapbox-gl.css'


import { stadGentApi } from '../../services/stadGentApi'
import { IApiError } from '../../types/apiError'
import { IBicycleOccupancy } from '../../types/bicycleOccupancy'
import { mapBicycleOccupancyToGeoJson } from '../../utils/mapToGeoJson'

import styles from './Map.module.css'


mapboxgl.accessToken = process.env.NEXT_PUBLIC_MAPBOX_ACCESS_TOKEN

const Map: React.FC = () => {
  const mapContainerReference = useRef<HTMLDivElement>(null)
  const mapboxMapReference = useRef<mapboxgl.Map | null>(null)
  const [markers, setMarkers] = useState<mapboxgl.Marker[]>()

  const [bicycleOccupancies, setBicycleOccupancies] = useState<IBicycleOccupancy[]>([])


  const [errorMessage, setErrorMessage] = useState<string>()
  const [errorSubMessage, setErrorSubMessage] = useState<string>()

  const fetchRealTimeBicycleOccupancies = (): void => {
    // reset error messages
    setErrorMessage('')
    setErrorSubMessage('')

    // fetch data
    stadGentApi.get<{ records: IBicycleOccupancy[] }>('?dataset=real-time-bezettingen-fietsenstallingen-gent&q=&facet=facilityname')
      // eslint-disable-next-line promise/always-return -- disable warning on this simple promise where the result is already used in the callback
      .then((response) => {
        setBicycleOccupancies(response.data.records)
      })
      // eslint-disable-next-line promise/prefer-await-to-callbacks -- disable warning on this simple promise where the async-await notation would not mean an improvement
      .catch((error: AxiosError<IApiError | undefined>) => {
        setErrorMessage(error.message)
        if (error.response?.data?.error !== undefined && error.response.data.error !== '') setErrorSubMessage(error.response.data.error)
      })
  }

  useEffect(() => {
    // start interval on mount
    const fetchInterval = setInterval(() => {
      fetchRealTimeBicycleOccupancies()
    }, 60 * 1000)

    // stop the interval on unmount
    return () => clearInterval(fetchInterval)
  }, [])

  // Initialize map when component mounts
  useEffect(() => {
    if (mapContainerReference.current) {
      const map = new mapboxgl.Map({
        center: [3.7477, 51.0548],
        container: mapContainerReference.current,
        style: 'mapbox://styles/mapbox/streets-v11',
        zoom: 11.7,
      })

      map.on('load', () => {
        // set reference to mapbox's map
        mapboxMapReference.current = map
        // fetch real-time data once already
        fetchRealTimeBicycleOccupancies()
        // TODO: set markers here, for an even better performance
      })

      // Clean up on unmount
      return () => map.remove()
    }
  }, [mapContainerReference, mapboxMapReference])

  // first draw map
  // meanwhile get data
  // on map and data ready => draw markers
  // keep track and disable making new markers

  useEffect(() => {
    if (mapboxMapReference.current !== null) {
      const bicycleOccupanciesAsGeoJson = {
        features: bicycleOccupancies.map((bicycleOccupancy) => mapBicycleOccupancyToGeoJson(bicycleOccupancy)),
        type: 'FeatureCollection',
      }

      if (markers && markers.length > 0) {
        markers.forEach((marker) => {
          const markerPopup: mapboxgl.Popup | undefined = marker.getPopup()
          // eslint-disable-next-line @typescript-eslint/no-unnecessary-condition -- false warning see https://github.com/typescript-eslint/typescript-eslint/issues/2511
          if (markerPopup !== undefined) {
            const matchingBicycleOccupancyData = bicycleOccupancies.find((bicycleOccupancy) => {
              const popupLngLat = markerPopup.getLngLat()
              return popupLngLat.lat === bicycleOccupancy.geometry.coordinates[1] && popupLngLat.lng === bicycleOccupancy.geometry.coordinates[0]
            })

            if (matchingBicycleOccupancyData) {
              markerPopup.setHTML(`<h3>${matchingBicycleOccupancyData.fields.facilityname}</h3>
                                  <span>Aantal beschikbare plaatsen: ${matchingBicycleOccupancyData.fields.freeplaces}</span><br/>
                                  <span>Totaal aantal plaatsen: ${matchingBicycleOccupancyData.fields.totalplaces} (${matchingBicycleOccupancyData.fields.occupiedplaces} bezet)</span>
                                  <p>Laatste update: ${new Date(matchingBicycleOccupancyData.fields.time).toLocaleString()}</p>`)
            } else {
              markerPopup.setHTML(`<h2>Geen informatie beschikbaar</h2>`)
            }
          }
        })
      } else {
      // make a marker for each feature and add it to the map
        const addedMarkers = bicycleOccupanciesAsGeoJson.features.map((geoJsonObject) => {
          const marker = new mapboxgl.Marker()
            .setLngLat(geoJsonObject.geometry.coordinates as [number, number])
            .addTo(mapboxMapReference.current!)

          const matchingBicycleOccupancyData = bicycleOccupancies.find((bicycleOccupancy) => bicycleOccupancy.fields.id === geoJsonObject.id)

          if (matchingBicycleOccupancyData) {
            marker.setPopup(new mapboxgl.Popup({
              offset: 40,
            }).setHTML(`<h3>${matchingBicycleOccupancyData.fields.facilityname}</h3>
                        <span>Aantal beschikbare plaatsen: ${matchingBicycleOccupancyData.fields.freeplaces}</span><br/>
                        <span>Totaal aantal plaatsen: ${matchingBicycleOccupancyData.fields.totalplaces} (${matchingBicycleOccupancyData.fields.occupiedplaces} bezet)</span>
                        <p>Laatste update: ${new Date(matchingBicycleOccupancyData.fields.time).toLocaleString()}</p>`))
          } else {
            marker.setPopup(new mapboxgl.Popup({
              offset: 40,
            }).setHTML(`<h2>Geen informatie beschikbaar</h2>`))
          }

          return marker
        })

        // keep track of the added markers
        setMarkers(addedMarkers)
      }
    }
  }, [
    bicycleOccupancies,
    mapboxMapReference,
    setMarkers,
    markers,
  ])

  return (
    <div>
      {
        typeof errorMessage !== 'undefined' && errorMessage !== '' && (
          <div className={styles['error-message']}>
            <strong>{errorMessage}</strong>
            <br />
            <span>{errorSubMessage}</span>
          </div>
        )
      }
      <div
        ref={mapContainerReference}
        className={styles['map-container']}
      />
    </div>
  )
}

export default Map
