import styles from './Footer.module.css'


const Footer: React.FC = () => (

  <footer className={styles.footer}>
    <span className={styles['footer-madeby']}>
      Gemaakt door Matthias Debaere in opdracht van Be-Mobile.
    </span>

    <span className={styles['footer-title']}><strong>Fietsenstallingen Gent</strong></span>

    <a
      className={styles['footer-sourcecode']}
      href='https://bitbucket.org/matthias_debaere/fietsenstallingen-gent/'
      rel='noopener noreferrer'
      target='_blank'
    >
      Source code
    </a>
  </footer>
)

export default Footer
