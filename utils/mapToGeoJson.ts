import { IBicycleOccupancy } from '../types/bicycleOccupancy'


export const mapBicycleOccupancyToGeoJson = (bicycleOccupancy: IBicycleOccupancy): GeoJSON.Feature<GeoJSON.Point> => ({
  geometry: bicycleOccupancy.geometry,
  id: bicycleOccupancy.fields.id,
  properties: {
    name: 'Bicycle Occupancy GeoJson',
  },
  type: 'Feature',
})
