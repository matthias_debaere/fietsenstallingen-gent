import { mapBicycleOccupancyToGeoJson } from './mapToGeoJson'


describe('mapToGeoJson utils', () => {
  it('should map BicycleOccupancy to GeoJson', () => {
    const actual = mapBicycleOccupancyToGeoJson({
      datasetid: 'real-time-bezettingen-fietsenstallingen-gent',
      fields: {
        facilityname: 'Braunplein',
        freeplaces: 90,
        id: '48-2',
        locatie: [51.054_029_332_9, 3.723_774_931_58],
        occupiedplaces: 26,
        time: '2021-03-10T10:25:00+00:00',
        totalplaces: 116,
      },
      geometry: {
        coordinates: [3.723_774_931_58, 51.054_029_332_9],
        type: 'Point',
      },
      record_timestamp: '2021-03-10T10:25:00.543000+00:00',
      recordid: '5a84331b2ddbca4ef0196f4a3bbfc1047c0ef021',
    })

    const expected = {
      geometry: {
        coordinates: [3.723_774_931_58, 51.054_029_332_9],
        type: 'Point',
      },
      id: '48-2',
      properties: {
        name: 'Bicycle Occupancy GeoJson',
      },
      type: 'Feature',
    }

    expect(actual).toStrictEqual(expected)
  })
})
