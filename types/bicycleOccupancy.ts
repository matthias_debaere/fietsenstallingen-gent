export interface IBicycleOccupancy {
  datasetid: string
  recordid: string
  fields: {
    occupiedplaces: number
    id: string
    facilityname: string
    time: string
    freeplaces: number
    locatie: [lat: number, long: number]
    totalplaces: number
  }
  geometry: {
    type: GeoJSON.Point['type']
    coordinates: GeoJSON.Point['coordinates'] // ! Important note: the received data has the longitude and latitude in reversed order here (as [long: number, lat: number], see https://en.wikipedia.org/wiki/ISO_6709#Items)
  }
  // eslint-disable-next-line @typescript-eslint/naming-convention -- Disabled because it is the naming convention of the api which we can't change
  record_timestamp: string
}

