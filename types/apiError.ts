/* eslint-disable @typescript-eslint/naming-convention -- Disabled because it is the naming convention of the api which we can't change */

export interface IApiError {
  errorcode: number // e.g. 10002,
  reset_time: string // e.g. "2021-01-26T00:00:00Z",
  limit_time_unit: string // e.g. "day",
  call_limit: number // e.g. 10000,
  error: string // e.g. "Too many requests on the domain. Please contact the domain administrator."
}
