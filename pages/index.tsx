import Head from 'next/head'

import Footer from '../components/footer/Footer'
import Map from '../components/map/Map'
import styles from '../styles/Home.module.css'


const Home: React.FC = () => (
  <div className={styles.container}>
    <Head>
      <title>Fietsenstallingen Gent</title>
      <link
        href='/favicon.ico'
        rel='icon'
      />
    </Head>

    <main className={styles.main}>
      {' '}
      <Map />
      {' '}
    </main>

    <Footer />
  </div>
)

export default Home
