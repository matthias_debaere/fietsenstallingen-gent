import { AppProps } from 'next/app'
import '../styles/globals.css'


const MyApp: React.FC<AppProps> = ({
  Component, pageProps,
  // eslint-disable-next-line react/jsx-props-no-spreading -- disable warning as pageProps are unknown/any and we can not explicitly parse each prop
}) => <Component {...pageProps} />

export default MyApp
