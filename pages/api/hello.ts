import { NextApiRequest,
         NextApiResponse } from 'next'


const handler = (_request: NextApiRequest, response: NextApiResponse): void => {
  response.status(200).json({
    name: 'John Doe',
  })
}

export default handler
